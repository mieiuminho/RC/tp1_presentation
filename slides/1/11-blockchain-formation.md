Introdução e Contextualização

![Estrutura da Blockchain](./images/blockchain-formation.svg) <!-- .element: align="right" width="250px" -->

# Estrutura da Blockchain

A cadeia principal (preta) consiste na maior série de blocos a partir do
bloco de gênese (verde) até ao bloco atual. Blocos órfãos (roxos) existem
fora da cadeia principal.

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

>>> *Fonte:* <https://upload.wikimedia.org/wikipedia/commons/9/98/Blockchain.svg>


Notes:

A blockchain é uma lista de registos em constante crescimento. Cada registo é chamado de bloco e estão ligados entre eles usando criptografia.

