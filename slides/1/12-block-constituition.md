Introdução e Contextualização

![Dados da Blockchain](./images/block-data.svg) <!-- .element: align="right" width="650px" -->


# Constituição de um bloco

- dados
- timestamp
- hash
- hash anterior

<br>
<br>
<br>
<br>

>>> *Fonte:* <https://upload.wikimedia.org/wikipedia/commons/5/55/Bitcoin_Block_Data.svg>

Notes:

Each block contains a cryptographic hash of the previous block, a timestamp,
and transaction data (generally represented as a Merkle tree).

Hash trees allow efficient and secure verification of the contents of large
data structures. Hash trees are a generalization of hash lists and hash chains.

Demonstrating that a leaf node is a part of a given binary hash tree requires
computing a number of hashes proportional to the logarithm of the number of
leaf nodes of the tree;
