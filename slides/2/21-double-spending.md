Desafios Associados

# Problema do duplo gasto

A invenção da blockchain para bitcoin fez dela a primeira moeda digital a
resolver o problema dos gastos duplos sem a necessidade de uma autoridade
confiável ou servidor central.

![Esquema transacional](./images/double-spending.jpg) <!-- .element: class="center"-->

>>> *Fonte:* <https://www.cs.bham.ac.uk/~mdr/teaching/modules06/netsec/lectures/digitalcash.html>

Notes:

