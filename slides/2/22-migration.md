Desafios Associados

# Migração das Tecnologias Atuais

![Migration](./images/migration.png) <!-- .element: width="600px" align="right" -->

- elevado custo
- problemas de escalabilidade
- falta de interesse por parte das instituições financeiras

Notes:

Um dos desafios das blockchains é a migração do sistema atual para um novo
sistema, como é o caso. Atualmente todas as transações financeiras são mediadas
por bancos e toda a informação sobre as mesmas está centralizada e por isso os
bancos possuem total controlo sobre elas e também beneficiam com taxas em todas
as transações que são feitas, podemos ver que fazer com que os bancos pretendam
adotar esta nova tecnologia não irá ser fácil pois implica uma potencial grande
queda no rendimento obtido assim como uma perda de poder que é algo que eles
não querem perder. Sendo assim a resistência por parte de grandes instituições,
não só bancos, será um grande travão ao avanço, mas a tecnologia existente
também é algo que preocupa, pois fazer a migração da informação existente em
todo o sistema financeiro para um novo sistema iria ser algo que requereria um
grande esforço computacional assim como uma grande capacidade destas novas
redes de suportar uma quantidade tao grande de informação partilhada por vários
computadores na rede. Também o grande consumo de energia que é necessário para
manter uma rede deste tipo é ainda uma grande preocupação devido ao elevado
grau de potencia computacional precisa para validar um novo bloco, se este for
validado pelo método PoW principalmente, o que é de facto um grande problema
tanto a nível financeiro como a nível ambiental.

