Desafios Associados

# Privacidade vs Transparência

![Privacy](./images/privacy.png) <!-- .element: width="600px" align="right" -->

Existe um _tradeoff_ entre a transparência das transações e a privacidade dos
agentes envolvidos na transação.


Notes:

Outro desafio é a privacidade dos utilizadores quando comparada com a
transparência das transações, pois o objetivo de uma blockchain é tornar todas
as transações visíveis para toda a gente para assim aumentar o grau de
transparência o que pode ser visto por alguns como uma invasão de privacidade.
Este tipo de problema é algo que é totalmente da responsabilidade de cada
blockchain, pois uma blockchain pode ser tão privada como se quiser pois
existem blockchain em que toda a informação é encriptada de forma a tudo ser
privado, o que é algo que pode facilitar atividade criminosa o que seria
claramente uma desvantagem, ou então completamente pública o que seria uma
invasão de privacidade de cada utilizador e é por isso que é necessário um
grande equilíbrio entre estas coisas e devem ser definidas em cada blockchain
de modo a melhorar a funcionalidade da blockchain conforme o que for
pretendido.

