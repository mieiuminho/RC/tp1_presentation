Validação dos Blocos

# _Proof of Work_

- Para adicionar cada bloco à cadeia, os mineradores devem competir para resolver um quebra-cabeça difícil usando o poder de processamento de seus computadores.
- Para adicionar um bloqueio malicioso, você precisa ter um computador mais poderoso que 51% da rede
- O primeiro mineiro a resolver o quebra-cabeça recebe uma recompensa por seu trabalho

Notes:

O método de validação usado nas bitcoins é o PoW(Proof of Work), que consiste
em cada transação necessita de ser verificada atraves de resolução de problemas
matematicos complexos que verifiquem a sua legitimidade, esta solução é
encontrada apenas usando brute force para resolver o que leva a que apenas se
consiga aumentar a quantidade de validações efetuadas tendo um maior poder
computacional, o que leva aos tais grandes consumos de energia. Assim que uma
transação é validada ela passa a ser um bloco na rede e o primeiro utilizador
que conseguiu resolver o problema é recompensada por isso.
