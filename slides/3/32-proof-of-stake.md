Validação dos Blocos

# _Proof of Stake_

- Não há concorrência, pois o criador do bloco é escolhido por um algoritmo baseado no capital do utilizador
- Para adicionar um bloco malicioso, o utilizador teria que possuir 51% de toda a criptomoeda na rede
- Não há recompensa por criar um bloco, portanto o criador do bloco cobra uma taxa de transação

Notes:

A validação PoS (Proof of Stake), tem o mesmo objetivo que a validação PoW,
passar transações a blocos e adicionalos a rede, contudo o metodo é bastante
diferente, em vez da validação depender de poder computacional dependia do
julgamento de um utilizador. O utilizador que iria decidir se a transação é
valida ou não é decidido através de um algoritmo em que os utilizadores que
possuem mais moedas/tokens são as que tem maior probabilidade de validar a
transação, o que acaba por fazer sentido pois são os que têm mais a perder caso
validem uma transação falsa, é facilmente preceptivel que este método gasta
muito menos energia do ponto de vista computacional o que é um ponto muito
positivo sobre este metodo. Contudo ao contrario do PoW em que os utilizadores
sao recompensados com novos moedas aqui os utilizadores sao recompensados com
taxas aplicadas a cada transação e por isso o numero de moedas/tokens na rede
não aumenta da mesma forma.

